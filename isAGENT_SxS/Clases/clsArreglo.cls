VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsArreglo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mArreglo()                                                              As Variant
Private mVarIndice                                                              As Long

'FILO primero en entrar ultimo en salir, arreglo o pila solo agrego al final y elimino el ultimo q entro

Property Get Arreglo()
    Arreglo = mArreglo
End Property

Public Function NumItems() As Long
    NumItems = mVarIndice
End Function

Public Sub AddItem(pItm As Variant)
    ReDim Preserve mArreglo(mVarIndice)
    mArreglo(mVarIndice) = pItm
    mVarIndice = mVarIndice + 1
End Sub

Public Sub RemoveItem()
    
    mVarIndice = mVarIndice - 1
    
    If mVarIndice <= 0 Then
        mVarIndice = 0
        ReDim mArreglo(mVarIndice)
    Else
        ReDim Preserve mArreglo(mVarIndice - 1)
    End If
    
End Sub

Private Sub Class_Initialize()
    mVarIndice = 0
    ReDim mArreglo(mVarIndice)
End Sub
