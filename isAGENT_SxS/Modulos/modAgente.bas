Attribute VB_Name = "modAgente"
Declare Function ExitWindowsEx& Lib "user32" (ByVal uFlags&, ByVal dwReserved&)

Private Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long) As Long

Private Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long

Private Declare Function GetComputerName Lib "kernel32" _
    Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, _
ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Enum eEstadoSucursal
    eSucNoActiva
    eSucActiva
End Enum

Enum eTipoTablaSincro
    etblMaestra
    etblTransaccion
    etblPendiente
End Enum

Enum eTipoCampo
    etblBusqueda
    etblExcepcion
End Enum

Enum eTipoError
    ErrGenerando
    ErrActualizando
    ErrOtros
End Enum

Enum eTipoActualizacion
    etaSoloAdm
    etaAdmPos
    etaSoloPos
End Enum

Enum eModalidad
    RecibeActualizaciones = 1
    EnviaRegistrosVentas = 2
End Enum

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Type StrucVentas
    sArchivo                                                                            As String
    sServidor                                                                           As String
    sUsuario                                                                            As String
    sPassword                                                                           As String
    sBD                                                                                 As String
    sTblVenta                                                                           As String
    sTblItems                                                                           As String
    sTblDetPago                                                                         As String
    sTblDetVuelto                                                                       As String
    sConnectTimeOut                                                                     As Long
    sCommandTimeOut                                                                     As Long
    Conexion                                                                            As ADODB.Connection
    RelacionGrupoFormaPago                                                              As Collection
    RelacionCodigoFormaPago                                                             As Collection
    RelacionCodigoBanco                                                                 As Collection
    RelacionCodigoMoneda                                                                As Collection
    RelacionCodigoMonedaISO                                                             As Collection
    sCodSucursal                                                                        As String
    bCodSucursalAlfanumerico                                                            As Boolean
    nRegistrosLote                                                                      As Long
    TransferirAutorizaciones                                                            As Boolean
    TransferirCierres                                                                   As Boolean
    TransferirAvancedeEfectivo                                                          As Boolean
    TransferirDonaciones                                                                As Boolean
    PLUCodeAlterno                                                                      As Boolean
    CodigosDeProductoAlfanumericos                                                      As Boolean
    ValidarDocumentoFiscal                                                              As Boolean
    TaxList1                                                                            As Collection
    TaxList2                                                                            As Collection
    TaxList3                                                                            As Collection
    DenominacionPagoPorAnticipo                                                         As String
    EnviarInformacionPuntoIntegrado_Tipo                                                As Integer
    EnviarInformacionPuntoIntegrado_PrefijoNovared                                      As String
    HabilitarProcesoHabladores                                                          As Boolean
    DetallarFormaPagoMultimoneda                                                        As Boolean
    IncluirCamposEmailTelefonoEnVentas                                                  As Boolean
    ModalidadCalculoDiscountRate                                                        As Integer
    EnviarTipoDescuentoItem_Modalidad                                                   As Integer
    EnviarTipoDescuentoItem_Modalidad1_TipoPrecio                                       As Integer
    EnviarTipoDescuentoItem_Modalidad1_TipoPrecioPrincipal                              As Integer
    DevAdm_EnviarMotivo_Modalidad                                                       As Integer
    DevAdm_EnviarDocRel_DevManual                                                       As Boolean
    SincronizarDesdeCentral                                                             As Boolean
    ReferenciarFormasDePagoEnDevolucion                                                 As Boolean
    ReferenciarFormasDePagoEnDevolucion_RelacionCodigoFormaPago                         As Collection
    ReferenciarFormasDePagoEnDevolucion_RelacionGrupoFormaPago                          As Collection
    FormasDePagoRestarVuelto                                                            As Boolean
    ReferenciarFormasDePagoEnDevolucion_FormasDePagoRestarVuelto                        As Boolean
End Type

Type StrucBalanza
    Habilitado                                                                          As Boolean
    sArchivo                                                                            As String
    sServidor                                                                           As String
    sUsuario                                                                            As String
    sPassword                                                                           As String
    sBD                                                                                 As String
    sTblProductos                                                                       As String
    sConnectTimeOut                                                                     As Long
    sCommandTimeOut                                                                     As Long
    sCodigoIni                                                                          As Variant
    sCodigoFin                                                                          As Variant
    Conexion                                                                            As ADODB.Connection
    RsPendCodigos                                                                       As ADODB.Recordset
    RsDataProductos                                                                     As ADODB.Recordset
    sDescripcionPLU                                                                     As String
    DecimalesCampoPrecioPLU                                                             As Integer
    TruncarPrecioPLU                                                                    As Boolean
    nRegistrosLote                                                                      As Long
    UsarDescripcionCorta                                                                As Boolean
    BalanzaCodigoAlterno                                                                As Boolean
End Type

Type StrucConex
    sModalidad                                                                          As String
    sArchivo                                                                            As String
    sServidor                                                                           As String
    sUsuario                                                                            As String
    sPassword                                                                           As String
    sBD                                                                                 As String
    sBDAdm                                                                              As String
    sBDPos                                                                              As String
    sConnectTimeOut                                                                     As Long
    sCommandTimeOut                                                                     As Long
    mConexion                                                                           As ADODB.Connection
    nRegistrosLote                                                                      As Long
End Type

Type StrucActualizaciones
    sArchivo                                                                            As String
    ManejaPOS                                                                           As Boolean
    HabilitarProcesoHabladores                                                          As Boolean
    ProcesarSinDescartarCambios                                                         As Boolean
    TopCantActualizacionesXLote                                                         As Long
    ActualizaHistoricoDeMonedas                                                         As Boolean
End Type

Private mClsTmp As Object

Public mEstrucConex                                                                     As StrucConex
Public mEstrucBalanza                                                                   As StrucBalanza
Public mEstrucVentas                                                                    As StrucVentas
Public mEstrucActualizaciones                                                           As StrucActualizaciones

Public gSucursalPrincipal                                                               As String
Public gEquipo                                                                          As String
Public gCarpetaDatos                                                                    As String
Public gDec                                                                             As String ' SystemDecSymbol
Public CodMonedaDestino                                                                 As String
Public MonedaDecimales                                                                  As Integer
Public AgruparProductosTicket                                                           As Boolean
Public DocumentUID_Sequence                                                             As Boolean

Global BD_ADM As String
Global BD_POS As String

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

Const SWP_NOSIZE = &H1
Const SWP_NOMOVE = &H2
Const SWP_NOZORDER = &H4
Const SWP_NOREDRAW = &H8
Const SWP_NOACTIVATE = &H10
Const SWP_DRAWFRAME = &H20
Const SWP_SHOWWINDOW = &H40
Const SWP_HIDEWINDOW = &H80
Const SWP_NOCOPYBITS = &H100
Const SWP_NOREPOSITION = &H200

'***************************************** Rutinas Api ******************************************************************

Public Function ObtenerConfiguracion(sIniFile As String, sSection As String, _
sKey As String, sDefault As String) As String
    
    Dim sTemp As String * 10000
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, 9999, sIniFile)
    
    ObtenerConfiguracion = Left$(sTemp, nLength)
    
End Function

Public Sub EscribirConfiguracion(sIniFile As String, sSection As String, _
sKey As String, sDefault As String)
    WritePrivateProfileString sSection, sKey, sDefault, sIniFile
End Sub

Public Function CadenasIguales(pCad1 As String, pCad2 As String) As Boolean
    CadenasIguales = Trim(UCase(pCad1)) = Trim(UCase(pCad2))
End Function

Public Function ObtenerNombreEquipo() As String
    
    Dim sEquipo As String * 255
    
    GetComputerName sEquipo, 255
    ObtenerNombreEquipo = Replace(sEquipo, Chr(0), "")
    
End Function

Public Function MoverArchivo(pOrigen As String, pDestino As String) As Boolean
    'Debug.Print pOrigen & "->" & pDestino
    MoverArchivo = MoveFile(pOrigen, pDestino)
End Function

Public Function CopiarArchivo(pOrigen As String, pDestino As String, pSobreEscribir As Boolean)
    'MsgBox pOrigen & "->" & pDestino
    CopiarArchivo = CopyFile(pOrigen, pDestino, IIf(pSobreEscribir, 0, 1))
End Function

'***************************************************************Metodos Principales ***************************************

Sub Main()
    
    DebugMode = Dir(App.Path & "\DebugMode.Active", vbArchive) <> vbNullString
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    gDec = SystemDecSymbol
    
    IniciarConfiguracion
    IniciarAgente
    
End Sub

Private Sub IniciarConfiguracion()
    
    With mEstrucConex
        
        .sArchivo = App.Path & "\Setup.ini"
        
        .sModalidad = Val(ObtenerConfiguracion(.sArchivo, "SERVER", "Modalidad", "1"))
        
        ' -------- Configuraci�n de SRV_STELLAR
        
        .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "SERVER", "CommandTimeOut", "30"))
        .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "SERVER", "ConnectionTimeOut", "15"))
        
        .sServidor = ObtenerConfiguracion(.sArchivo, "SERVER", "SRV_LOCAL", "")
        
        .sBDAdm = ObtenerConfiguracion(.sArchivo, "SERVER", "BD_ADM", "VAD10")
        .sBDPos = ObtenerConfiguracion(.sArchivo, "SERVER", "BD_POS", "VAD20")
        
        BD_ADM = .sBDAdm
        BD_POS = .sBDPos
        
        If .sModalidad = RecibeActualizaciones Then
            .sBD = .sBDAdm
        ElseIf .sModalidad = EnviaRegistrosVentas Then
            .sBD = .sBDPos
        End If
        
        .sUsuario = ObtenerConfiguracion(.sArchivo, "SERVER", "User", "SA")
        .sPassword = ObtenerConfiguracion(.sArchivo, "SERVER", "Password", "")
        
        If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If Not mClsTmp Is Nothing Then
                .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
            End If
            
        End If
        
        If Not DebugMode Then
            DebugMode = Val(ObtenerConfiguracion(.sArchivo, "SERVER", "DebugMode", "0")) = 1
        End If
        
    End With
    
    If mEstrucConex.sModalidad = RecibeActualizaciones Then
        
        With mEstrucActualizaciones
            
            .sArchivo = mEstrucConex.sArchivo
            
            .ManejaPOS = (Val(ObtenerConfiguracion(.sArchivo, _
            "ACTUALIZACIONES", "ManejaPOS", "1")) = 1)
            
            .HabilitarProcesoHabladores = (Val(ObtenerConfiguracion(.sArchivo, _
            "ACTUALIZACIONES", "HabilitarProcesoHabladores", "0")) = 1)
            
            .ProcesarSinDescartarCambios = (Val(ObtenerConfiguracion(.sArchivo, _
            "ACTUALIZACIONES", "ProcesarSinDescartarCambios", "1")) = 1)
            
            .TopCantActualizacionesXLote = Val(ObtenerConfiguracion(.sArchivo, _
            "ACTUALIZACIONES", "TopCantActualizacionesXLote", "10")) ' Manejar Pedacitos muy peque�os de informaci�n
            ' por lote para tratar de asegurar el funcionamiento continuo y evitar perdida de datos.
            
            .ActualizaHistoricoDeMonedas = (Val(ObtenerConfiguracion(.sArchivo, _
            "ACTUALIZACIONES", "ActualizaHistoricoDeMonedas", "1")) = 1)
            
        End With
        
        With mEstrucBalanza
            
            .sArchivo = mEstrucConex.sArchivo
            
            ' -------- Configuraci�n de SRV_BALANZA
            .Habilitado = Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "Habilitado", "0")) = 1 ' Mientras se certifica manejando otras prioridades.
            
            If .Habilitado Then
                
                .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "CommandTimeOut", "-1"))
                .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "ConnectionTimeOut", "-1"))
                
                If .sCommandTimeOut = -1 Then .sCommandTimeOut = mEstrucConex.sCommandTimeOut
                If .sConnectTimeOut = -1 Then .sConnectTimeOut = mEstrucConex.sConnectTimeOut
                
                .sServidor = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "SRV_LOCAL", vbNullString)
                If .sServidor = vbNullString Then .sServidor = mEstrucConex.sServidor
                
                .sBD = UCase(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "BD", "CAS"))
                .sTblProductos = UCase(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "Objeto_Info_Productos", "BLZ_ARTICULO_CAS"))
                
                .sUsuario = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "User", vbNullString)
                .sPassword = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "Password", vbNullString)
                
                If .sUsuario = vbNullString And .sPassword = vbNullString Then
                    .sUsuario = mEstrucConex.sUsuario
                    .sPassword = mEstrucConex.sPassword
                End If
                
                If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
                    
                    Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
                    
                    If Not mClsTmp Is Nothing Then
                        .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                        .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
                    End If
                    
                End If
                
                .sDescripcionPLU = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "DescPLU", "PLU")
                
                .sCodigoIni = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "CodigoIni", "20000")
                .sCodigoFin = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "CodigoFin", "29999")
                
                If IsNumeric(.sCodigoIni) And IsNumeric(.sCodigoFin) Then
                    .sCodigoIni = CLng(SVal(.sCodigoIni))
                    .sCodigoFin = CLng(SVal(.sCodigoFin))
                Else
                    .sCodigoIni = CStr(.sCodigoIni)
                    .sCodigoFin = CStr(.sCodigoFin)
                End If
                
                .nRegistrosLote = CLng(Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "RegistrosLote", "-1")))
                If (.nRegistrosLote <= 0) Then .nRegistrosLote = 250
                mEstrucConex.nRegistrosLote = .nRegistrosLote
                
                .UsarDescripcionCorta = (Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "DescripcionCorta", "0")) = 1)
                
                .DecimalesCampoPrecioPLU = Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "DecimalesCampoPrecioPLU", "1"))
                If .DecimalesCampoPrecioPLU < 0 Then .DecimalesCampoPrecioPLU = 0
                
                .TruncarPrecioPLU = (Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "TruncarPrecioPLU", "0")) = 1)
                
                .BalanzaCodigoAlterno = (Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "BalanzaCodigoAlterno", "0")) = 1)
                
            End If
            
        End With
        
    ElseIf mEstrucConex.sModalidad = EnviaRegistrosVentas Then
        
        With mEstrucVentas
            
            .sArchivo = mEstrucConex.sArchivo
            
            ' -------- Configuraci�n de SRV_VENTAS
            
            .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CommandTimeOut", "-1"))
            .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "ConnectionTimeOut", "-1"))
            
            If .sCommandTimeOut = -1 Then .sCommandTimeOut = mEstrucConex.sCommandTimeOut
            If .sConnectTimeOut = -1 Then .sConnectTimeOut = mEstrucConex.sConnectTimeOut
            
            .sServidor = ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "SRV_LOCAL", vbNullString)
            If .sServidor = vbNullString Then .sServidor = mEstrucConex.sServidor
            
            .sBD = UCase(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "BD", "VentasStore"))
            
            .sTblVenta = UCase(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CabeceroVentas", "TICKET"))
            .sTblItems = UCase(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "DetalleVentas", "TICKET_DETAIL"))
            .sTblDetPago = UCase(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "DetallePagoVentas", "MEDIA_PAY"))
            .sTblDetVuelto = UCase(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "DetalleVueltoVentas", "MEDIA_CHANGE"))
            
            .sCodSucursal = ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CodSucursal", Empty) ' Valores posibles -1 o Distinto.
            .bCodSucursalAlfanumerico = Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CodSucursalAlfanumerico", "0")) = 1
            
            If Not .bCodSucursalAlfanumerico Then
                If Val(.sCodSucursal) = 0 Then
                    Call EscribirLog("Sucursal de venta inv�lida / no establecida.")
                    End
                End If
            Else
                If Trim(.sCodSucursal) = Empty Then
                    Call EscribirLog("Sucursal de venta inv�lida / no establecida.")
                    End
                End If
            End If
            
            .DetallarFormaPagoMultimoneda = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", _
            "DetallarFormaPagoMultimoneda", "0")) = 1)
            
            Set .RelacionGrupoFormaPago = ConvertirCadenaDeAsociacion(ObtenerConfiguracion( _
            .sArchivo, "ACT_VENTAS", "GruposFormaPago", vbNullString), pKeysCaseOption:=2)
            
            If .RelacionGrupoFormaPago Is Nothing Then
                Call EscribirLog("Datos de asociaciones de Grupo de Formas de Pago inv�lidos / no establecidos")
                End
            End If
            
            Set .RelacionCodigoFormaPago = ConvertirCadenaDeAsociacion(ObtenerConfiguracion( _
            .sArchivo, "ACT_VENTAS", "CodigosFormaPago", vbNullString), pKeysCaseOption:=2)
            
            If .RelacionCodigoFormaPago Is Nothing Then
                Call EscribirLog("Datos de asociaciones de C�digo de Formas de Pago inv�lidos / no establecidos")
                End
            End If
            
            mStrRelacionCodigoBanco = ObtenerConfiguracion( _
            .sArchivo, "ACT_VENTAS", "BancosFormaPago", vbNullString)
            
            If Trim(mStrRelacionCodigoBanco) <> Empty Then
                
                Set .RelacionCodigoBanco = ConvertirCadenaDeAsociacion(mStrRelacionCodigoBanco, pKeysCaseOption:=2)
                
                If .RelacionCodigoBanco Is Nothing Then
                    Call EscribirLog("Datos de asociaciones de C�digo de Banco para las Formas de Pago inv�lidos / no establecidos")
                    End
                End If
                
            End If
            
            Set .RelacionCodigoMoneda = ConvertirCadenaDeAsociacion( _
            ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CodigosMoneda", "*:*"), pKeysCaseOption:=2)
            
            If .RelacionCodigoMoneda Is Nothing Then
                Call EscribirLog("Datos de asociaciones de C�digo de Monedas inv�lidos / no establecidos")
                End
            End If
            
            Set .RelacionCodigoMonedaISO = ConvertirCadenaDeAsociacion( _
            ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CodigosMonedaISO", "*:*"), pKeysCaseOption:=2)
            
            If .RelacionCodigoMonedaISO Is Nothing Then
                Set .RelacionCodigoMonedaISO = New Collection
            End If
            
            Set .TaxList1 = ConvertirCadenaDeAsociacion(ObtenerConfiguracion( _
            .sArchivo, "ACT_VENTAS", "TaxList1", "Tax1:12.00"))
            
            Set .TaxList2 = ConvertirCadenaDeAsociacion(ObtenerConfiguracion( _
            .sArchivo, "ACT_VENTAS", "TaxList2", "Tax1:08.00"))
            
            Set .TaxList3 = ConvertirCadenaDeAsociacion(ObtenerConfiguracion( _
            .sArchivo, "ACT_VENTAS", "TaxList3", "Tax1:22.00|Tax2:07.00|Tax3:09.00|TaxOld:10.00"))
            
            .TransferirAutorizaciones = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "OperacionesAutorizadas", "0")) = 1)
            .ValidarDocumentoFiscal = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "ValidarDocumentoFiscal", "0")) = 1)
            
            .TransferirCierres = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "EnviarCierres", "0")) = 1)
            
            .TransferirAvancedeEfectivo = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "EnviarAvancedeEfectivo", "0")) = 1)
            
            .TransferirDonaciones = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "EnviarDonaciones", "0")) = 1)
            
             AgruparProductosTicket = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "AgruparProductos", "0")) = 1)
             
             DocumentUID_Sequence = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "DocumentUID_Sequence", "0")) = 1)
             
            .DenominacionPagoPorAnticipo = QuitarComillasSimples(ObtenerConfiguracion( _
            .sArchivo, "ACT_VENTAS", "DenominacionWeb", "Efectivo"))
            
            If Len(Trim(.DenominacionPagoPorAnticipo)) = 0 Then .DenominacionPagoPorAnticipo = "Efectivo"
            
            .EnviarInformacionPuntoIntegrado_Tipo = Val(ObtenerConfiguracion(.sArchivo, _
            "ACT_VENTAS", "EnviarInformacionPuntoIntegrado_Tipo", vbNullString))
            
            .EnviarInformacionPuntoIntegrado_PrefijoNovared = ObtenerConfiguracion(.sArchivo, _
            "ACT_VENTAS", "EnviarInformacionPuntoIntegrado_PrefijoNovared", "*NVRPM*:")
            
            .sUsuario = ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "User", vbNullString)
            .sPassword = ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "Password", vbNullString)
            
            If .sUsuario = vbNullString And .sPassword = vbNullString Then
                .sUsuario = mEstrucConex.sUsuario
                .sPassword = mEstrucConex.sPassword
            End If
            
            If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
                
                Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
                
                If Not mClsTmp Is Nothing Then
                    .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                    .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
                End If
                
            End If
            
            .nRegistrosLote = CLng(Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "RegistrosLote", "-1")))
            If (.nRegistrosLote <= 0) Then .nRegistrosLote = 100
            mEstrucConex.nRegistrosLote = .nRegistrosLote
            
            .PLUCodeAlterno = (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "PLUCodeAlterno", "0")) = 1)
            
            .CodigosDeProductoAlfanumericos = _
            (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CodigosDeProductoAlfanumericos", "0")) = 1)
            
            .IncluirCamposEmailTelefonoEnVentas = _
            (Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "IncluirCamposEmailTelefonoEnVentas", "0")) = 1)
            
            .ModalidadCalculoDiscountRate = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "ModalidadCalculoDiscountRate", "1"))
            
            CodMonedaDestino = ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "CodMonedaDestino", "*")
            MonedaDecimales = SDec(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "MonedaDecimales", "8"))
            
            .EnviarTipoDescuentoItem_Modalidad = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "EnviarTipoDescuentoItem_Modalidad", "0"))
            
            .EnviarTipoDescuentoItem_Modalidad1_TipoPrecio = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "EnviarTipoDescuentoItem_Modalidad1_TipoPrecio", "2"))
            
            .EnviarTipoDescuentoItem_Modalidad1_TipoPrecioPrincipal = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "EnviarTipoDescuentoItem_Modalidad1_TipoPrecioPrincipal", Empty))
            
            .DevAdm_EnviarMotivo_Modalidad = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "DevAdm_EnviarMotivo_Modalidad", "0"))
            
            .DevAdm_EnviarDocRel_DevManual = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "DevAdm_EnviarDocRel_DevManual", "0")) = 1
            
            .SincronizarDesdeCentral = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "SincronizarDesdeCentral", "0")) = 1
            
            .ReferenciarFormasDePagoEnDevolucion = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "ReferenciarFormasDePagoEnDevolucion", "0")) = 1
            
            If .ReferenciarFormasDePagoEnDevolucion Then
                
                Set .ReferenciarFormasDePagoEnDevolucion_RelacionCodigoFormaPago = ConvertirCadenaDeAsociacion(ObtenerConfiguracion( _
                .sArchivo, "ACT_VENTAS", "ReferenciarFormasDePagoEnDevolucion_GruposFormaPago", vbNullString), pKeysCaseOption:=2)
                
                If .ReferenciarFormasDePagoEnDevolucion_RelacionCodigoFormaPago Is Nothing Then
                    Call EscribirLog("Datos de asociaciones de Grupo de Formas de Pago de Devoluci�n inv�lidos / no establecidos")
                    End
                End If
                
                Set .ReferenciarFormasDePagoEnDevolucion_RelacionGrupoFormaPago = ConvertirCadenaDeAsociacion(ObtenerConfiguracion( _
                .sArchivo, "ACT_VENTAS", "ReferenciarFormasDePagoEnDevolucion_CodigosFormaPago", vbNullString), pKeysCaseOption:=2)
                
                If .ReferenciarFormasDePagoEnDevolucion_RelacionGrupoFormaPago Is Nothing Then
                    Call EscribirLog("Datos de asociaciones de C�digo de Formas de Pago de Devoluci�n inv�lidos / no establecidos")
                    End
                End If
                
            End If
            
            .FormasDePagoRestarVuelto = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "FormasDePagoRestarVuelto", "1")) = 1
            
            .ReferenciarFormasDePagoEnDevolucion_FormasDePagoRestarVuelto = _
            Val(ObtenerConfiguracion(.sArchivo, "ACT_VENTAS", "ReferenciarFormasDePagoEnDevolucion_FormasDePagoRestarVuelto", "0")) = 1
            
        End With
        
        With mEstrucBalanza ' Para grabar el campo PLUCode
            
            .sDescripcionPLU = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "DescPLU", "PLU")
            
            .sCodigoIni = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "CodigoIni", "20000")
            .sCodigoFin = ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "CodigoFin", "29999")
            
            If IsNumeric(.sCodigoIni) And IsNumeric(.sCodigoFin) Then
                .sCodigoIni = CLng(Val(.sCodigoIni))
                .sCodigoFin = CLng(Val(.sCodigoFin))
            Else
                .sCodigoIni = CStr(.sCodigoIni)
                .sCodigoFin = CStr(.sCodigoFin)
            End If
            
            .DecimalesCampoPrecioPLU = Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "DecimalesCampoPrecioPLU", "1"))
            If .DecimalesCampoPrecioPLU < 0 Then .DecimalesCampoPrecioPLU = 0
            
            .TruncarPrecioPLU = (Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "TruncarPrecioPLU", "0")) = 1)
            
            .BalanzaCodigoAlterno = (Val(ObtenerConfiguracion(.sArchivo, "ACT_BALANZA", "BalanzaCodigoAlterno", "0")) = 1)
            
        End With
        
    End If
    
    'gSucursalPrincipal = ObtenerConfiguracion(.sArchivo, "Entrada", "DefaultBranch", "")
    gEquipo = ObtenerNombreEquipo()
    'gCarpetaDatos = ObtenerConfiguracion(.sArchivo, "Entrada", "CarpetaDatos", App.Path & "\Datos\")
    
End Sub

Private Sub IniciarAgente()
    
    Dim mClsAgente As clsAgente, NewUser As String, NewPassword As String
    
    With mEstrucConex
        
        NewUser = vbNullString: NewPassword = vbNullString
        
        If EstablecerConexion(.mConexion, .sServidor, .sBD, _
        .sConnectTimeOut, , , .sUsuario, .sPassword, True, NewUser, NewPassword) Then
            If NewUser <> "" Or NewPassword <> "" Then
                EscribirConfiguracion .sArchivo, "SERVER", "User", NewUser
                EscribirConfiguracion .sArchivo, "SERVER", "Password", NewPassword
            End If
        End If
        
    End With
    
    If mEstrucConex.sModalidad = RecibeActualizaciones Then
        
        With mEstrucBalanza
            
            If .Habilitado Then
                
                NewUser = vbNullString: NewPassword = vbNullString
                
                If Not (CadenasIguales(mEstrucConex.sServidor, .sServidor) _
                And CadenasIguales(mEstrucConex.sUsuario, .sUsuario) _
                And CadenasIguales(mEstrucConex.sPassword, .sPassword)) _
                Then
                    If EstablecerConexion(.Conexion, .sServidor, .sBD, _
                    .sConnectTimeOut, , , .sUsuario, .sPassword, True, NewUser, NewPassword) Then
                        If NewUser <> "" Or NewPassword <> "" Then
                            EscribirConfiguracion .sArchivo, "ACT_BALANZA", "User", NewUser
                            EscribirConfiguracion .sArchivo, "ACT_BALANZA", "Password", NewPassword
                        End If
                    End If
                Else
                    Set .Conexion = mEstrucConex.mConexion
                End If
                
            End If
            
        End With
        
    ElseIf mEstrucConex.sModalidad = EnviaRegistrosVentas Then
        With mEstrucVentas
            
            NewUser = vbNullString: NewPassword = vbNullString
            
            If Not (CadenasIguales(mEstrucConex.sServidor, .sServidor) _
            And CadenasIguales(mEstrucConex.sUsuario, .sUsuario) _
            And CadenasIguales(mEstrucConex.sPassword, .sPassword)) _
            Then
                If EstablecerConexion(.Conexion, .sServidor, .sBD, _
                .sConnectTimeOut, , , .sUsuario, .sPassword, True, NewUser, NewPassword) Then
                    If NewUser <> "" Or NewPassword <> "" Then
                        EscribirConfiguracion .sArchivo, "ACT_VENTAS", "User", NewUser
                        EscribirConfiguracion .sArchivo, "ACT_VENTAS", "Password", NewPassword
                    End If
                End If
            Else
                Set .Conexion = mEstrucConex.mConexion
            End If
            
        End With
    End If
    
    With mEstrucConex
        If Not .mConexion Is Nothing Then
            If .mConexion.State = adStateOpen Then
                Set mClsAgente = New clsAgente
                mClsAgente.IniciarAgente .mConexion
                Set mClsAgente = Nothing
            End If
        End If
    End With
    
    End
    
End Sub

'*********************************************** Funciones de Apoyo ****************************************************************

Public Function EstablecerConexion(ByRef pCn As ADODB.Connection, pServidor As String, pBD As String, _
Optional pCnnTOut As Long = 15, Optional pCorrelativo As String, Optional pSucursal As String, _
Optional ByRef pUser, Optional ByRef pPassword, Optional AuthReset As Boolean = False, _
Optional ByRef NewUser As String, Optional ByRef NewPassword As String) As Boolean
    
Retry:
    
    On Error GoTo ErrHandler
    
    Dim mCadena As String
    Dim mServidor As String
    
    Set pCn = New ADODB.Connection
    pCn.ConnectionTimeout = pCnnTOut
    
    If IsMissing(pUser) Or IsMissing(pPassword) Then
        pCn.Open CadenaConexion(pServidor, pBD)
    Else
        pCn.Open CadenaConexion(pServidor, pBD, CStr(pUser), CStr(pPassword))
    End If
    
    EstablecerConexion = True
    
    Exit Function
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If AuthReset Then
        
        If Err.Number = -2147217843 Then
            
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If mClsTmp Is Nothing Then GoTo UnhandledErr
            
            TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
            
            If Not IsEmpty(TmpVar) Then
                pUser = TmpVar(0): pPassword = TmpVar(1)
                NewUser = TmpVar(2): NewPassword = TmpVar(3)
                Resume Retry
            End If
            
            Set mClsTmp = Nothing
            
        End If
        
    End If
    
UnhandledErr:
    
    GrabarErrorBD "EstablecerConexion", mErrDesc, mErrNumber, ErrOtros, pCorrelativo
    
End Function

Private Function CadenaConexion(ByVal pServidor As String, ByVal pBD As String, Optional ByVal pUser As String = "SA", Optional ByVal pPassword As String = "")
    CadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" & pBD & ";Data Source=" & pServidor & ";" _
    & IIf(pUser = vbNullString Or pPassword = vbNullString, _
    "Persist Security Info=False;User ID=" & pUser & ";", _
    "Persist Security Info=True;User ID=" & pUser & ";Password=" & pPassword & ";")
    'Debug.Print CadenaConexion
End Function

Public Function Correlativo(pCn As ADODB.Connection, pCampo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    Dim mValor As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_CORRELATIVOS " & _
    "WHERE (cu_Campo = '" & pCampo & "') "
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockPessimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_Valor = mRs!nu_Valor + 1
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
        mRs.Update
    Else
        mRs.AddNew
        mRs!cu_Campo = pCampo
        mRs!cu_Formato = "0000000000"
        mRs!nu_Valor = 1
        mRs.Update
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
    End If
    
    Correlativo = mValor
    
    Exit Function
    
Errores:
    
    EscribirLog "Error obteniendo correlativo: " & Err.Description, Err.Number
    
End Function

Public Function CorrelativoUnico(pCn As ADODB.Connection, pCampo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    Dim mValor As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_CORRELATIVOS " & _
    "WHERE (cu_Campo = '" & pCampo & "') "
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockPessimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_Valor = mRs!nu_Valor + 1
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
        mRs.Update
    Else
        mRs.AddNew
        mRs!cu_Campo = pCampo
        mRs!cu_Formato = "000000"
        mRs!nu_Valor = 1
        mRs.Update
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
    End If
    
    CorrelativoUnico = mValor
    
    Exit Function
    
Errores:
    
    EscribirLog "Error obteniendo correlativo: " & Err.Description, Err.Number
    
End Function

Public Function CorrelativoUnicoAnterior(pCn As ADODB.Connection, pCampo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    Dim mValor As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * From MA_CORRELATIVOS WHERE (CU_Campo = '" & pCampo & "')"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockPessimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_Valor = mRs!nu_Valor - 1
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
        mRs.Update
    Else
        mRs.AddNew
        mRs!cu_Campo = pCampo
        mRs!cu_Formato = "0000000000"
        mRs!nu_Valor = 1
        mRs.Update
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
    End If
    
    CorrelativoUnicoAnterior = mValor
    
    Exit Function
    
Errores:
    
    EscribirLog "Error obteniendo correlativo: " & Err.Description, Err.Number
    
End Function

Public Sub EscribirLog(ByVal pMensaje As String, Optional ByVal pNumError As Long)
    
    Dim mCanal As Integer
    Dim mArchivo As String
    
    mCanal = FreeFile()
    Open App.Path & "\Errores.log" For Append Access Write As #mCanal
    Print #mCanal, Now & ", " & pMensaje & IIf(pNumError <> 0, " (Error Numero: " & pNumError & ")", "")
    Close #mCanal
    
End Sub

Public Sub VerificarExisteCarpeta(pCarpeta As String)
    If Dir(pCarpeta, vbDirectory) = "" Then
        MkDir pCarpeta
    End If
End Sub

Public Sub GrabarErrorBD(ByVal pRutina As String, ByVal pError As String, _
ByVal pNError As Long, pTipo As eTipoError, Optional ByVal pCorrida As String, _
Optional ByVal pSucursal, Optional ByVal pTabla As String, Optional ByVal pCodigoDoc As String, _
Optional ByVal pConcepto As String, Optional ByVal pFecha As Date = "01/01/1900")
    
    Dim mSQL As String
    
    On Error GoTo Errores
        
    mSQL = "INSERT INTO MA_ERRORCORRIDAS_AGENTE_ADM (Equipo, Corrida, Error, nError, Sucursal, " _
    & "Tabla, Codigo, Concepto, Fecha, Rutina, Tipo) VALUES " _
    & "('" & gEquipo & "', '" & pCorrida & "', '" & Left(Replace(pError, "'", vbNullString), 120) & "', " & CStr(pNError) & ", '" & CStr(pSucursal) & "', " _
    & "'" & pTabla & "', '" & pCodigoDoc & "', '" & pConcepto & "', '" & FechaBD(pFecha, FBD_FULL) & "', '" & pRutina & "', " & CStr(pTipo) & ")"
    
    mEstrucConex.mConexion.Execute mSQL
   
    Exit Sub
    
Errores:
    
    modAgente.EscribirLog "Error Grabando Tabla Errores: " & Err.Description, Err.Number
    
    Err.Clear
    
End Sub

Public Function ExisteTabla(ByVal pTabla As String, pCn As ADODB.Connection, Optional ByVal pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTabla = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

Public Function ExisteCampoTabla(ByVal pColumna As String, ByVal pTabla As String, pCn As ADODB.Connection, _
Optional ByVal pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Column_Name FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS WHERE Table_Name = '" & pTabla & "' AND Column_Name = '" & pColumna & "'")
    ExisteCampoTabla = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

Public Function ExisteCampoRs(pRs As ADODB.Recordset, pCampo As String) As Boolean
    
    On Error GoTo Errores
    
    ValorTmp = pRs.Fields(pCampo).Name
    ExisteCampoRs = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Sub VentanaVisible(ByRef pFrm As Object, Optional pTop As Boolean = False)
    If pTop Then
        'SetWindowPos pFrm.hWnd, -1, pFrm.Left / Screen.TwipsPerPixelX, pFrm.Top / Screen.TwipsPerPixelY, pFrm.Width / Screen.TwipsPerPixelX, pFrm.Height / Screen.TwipsPerPixelY, SWP_NOACTIVATE Or SWP_NOSIZE Or SWP_NOMOVE
        SetWindowPos pFrm.hWnd, -1, 0, 0, 0, 0, SWP_NOACTIVATE Or SWP_NOSIZE
    Else
        SetWindowPos pFrm.hWnd, -2, 0, 0, 0, 0, SWP_NOACTIVATE Or SWP_NOSIZE
    End If
End Sub

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = Empty Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIO A LA DERECHA
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

' Imprime un recordset o el Record Actual utilizando el m�todo original de ADODB.

' El Original de ADODB pero incluyendo los headers.
' Es m�s r�pido pero menos ordenado.
' NO Deja el recordset en su posici�n del cursor original:
' Lo lleva al EOF si es el Recordset, o avanza el cursor una posici�n si es solo el Record Actual.
' Por lo cual habr�a que hacer MoveFirst o MovePrevious respectivamente
' Dependiendo del tipo de recorset para poder volver a recorrerlo.

Public Function QuickPrintRecordset(ByVal pRs As ADODB.Recordset, Optional OnlyCurrentRow As Boolean = False) As String
    
    On Error Resume Next
    
    Dim Data As Boolean
    Dim RsPreview As String
    Dim RowData As String
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    For i = 0 To pRs.Fields.Count - 1
        RsPreview = RsPreview + IIf(i = 0, vbNullString, "|") + pRs.Fields(i).Name
    Next i
    
    RsPreview = RsPreview & vbNewLine
    
    If Data Then
        If OnlyCurrentRow Then
            RowData = pRs.GetString(, 1, "|", vbNewLine, "NULL")
        Else
            RowData = pRs.GetString(, , "|", vbNewLine, "NULL")
        End If
    End If
    
    RsPreview = RsPreview & RowData
    
    QuickPrintRecordset = RsPreview
    
End Function

Public Function QuickPrintRecord(ByVal pRs As ADODB.Recordset) As String
    QuickPrintRecord = QuickPrintRecordset(pRs, True)
End Function

' Imprime un Recordset o el Record Actual de manera ordenada. Deja al recordset en su posici�n (Record) Original.

' Parametros:

' 1) pRs: Recordset
' 2) pRowNumAlias: Alias para una columna que enumera los rows. Si no se especifica o OnlyCurrentRow no se imprime
' 3) OnlyCurrentRow: Imprimir solo el record actual o todo.
' 4) EncloseHeaders: Encierra los cabeceros en Corchetes para mayor separaci�n.
' 5) LineBeforeData: Agrega una Linea de separaci�n entre Header y Records.
' 6) pOnValueError: Que se imprime si da un error a la hora de recuperar un campo.
' 7) Uso interno: Dejar siempre el valor default. Es para poder imprimir los Recordsets anidados (Tipo Consulta Shape).

' Recomendaci�n al obtener el String devuelto: Copiar y Pegar en TextPad o un Bloc de Notas sin Line Wrap para analizar.

Public Function PrintRecordset(ByVal pRs As ADODB.Recordset, Optional ByVal pRowNumAlias As String = vbNullString, _
Optional OnlyCurrentRow As Boolean = False, Optional EncloseHeaders As Boolean = True, _
Optional LineBeforeData As Boolean = False, Optional ByVal pOnValueError As String = "Error|N/A", _
Optional NestLevel As Byte = 0) As String
    
    On Error GoTo FuncError
    
    Dim RsPreview As String, bRowNumAlias As Boolean, RowNumAlias As String, RowNumCount As Double
    Dim OriginalRsPosition, TmpRsValue, mColLength() As Double, mColArrIndex, pRsValues As ADODB.Recordset
    Dim Data As Boolean
    
    If OnlyCurrentRow Then pRowNumAlias = vbNullString
    
    bRowNumAlias = (pRowNumAlias <> vbNullString)
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    ReDim mColLength(pRs.Fields.Count - 1 + IIf(bRowNumAlias, 1, 0))
    
    If bRowNumAlias Then
        RowNumAlias = IIf(EncloseHeaders, "[", vbNullString) & CStr(pRowNumAlias) & IIf(EncloseHeaders, "]", vbNullString)
        RowNumCount = 0
        RsPreview = RsPreview & GetTab(NestLevel) & "$(mCol[0])"
        mColLength(0) = Len(RowNumAlias)
    End If
    
    For i = 0 To pRs.Fields.Count - 1
        'RsPreview = RsPreview + pRs.Fields(i).Name + vbTab
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & "$(mCol[" & mColArrIndex & "])"
        mColLength(mColArrIndex) = Len(IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString))
    Next i
    
    If LineBeforeData Then RsPreview = RsPreview & vbNewLine
    
    If Data And Not OnlyCurrentRow Then
        OriginalRsPosition = SafeProp(pRs, "Bookmark", -1)
        If pRs.CursorType <> adOpenForwardOnly Then
            pRs.MoveFirst
        Else
            pRs.Requery
        End If
        If OriginalRsPosition <> -1 Then Set pRsValues = pRs.Clone(adLockReadOnly)
    Else
        Set pRsValues = pRs
    End If
    
    RowNumCount = 0
    
    Do While Not pRs.EOF
        
        'RsPreview = RsPreview & vbNewLine
        'If bRowNumAlias Then RsPreview = RsPreview & Rellenar_SpaceR(RowNumCount, Len(RowNumAlias), " ")
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            If Len(TmpRsValue) > mColLength(0) Then mColLength(0) = Len(TmpRsValue)
            'RsPreview = RsPreview & TmpRsValue
        End If
        
        For i = 0 To pRs.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRs, pRs.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
                        
            If UCase(TypeName(TmpRsValue)) <> UCase("Recordset") Then
                If Len(TmpRsValue) > mColLength(mColArrIndex) Then mColLength(mColArrIndex) = Len(TmpRsValue)
            End If
            'RsPreview = RsPreview & TmpRsValue
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRs.MoveNext
        
    Loop
    
    If bRowNumAlias Then RsPreview = Replace(RsPreview, "$(mCol[0])", Rellenar_SpaceR(RowNumAlias, mColLength(0), " ") & vbTab, , 1)
    
    For i = 0 To pRs.Fields.Count - 1
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = Replace(RsPreview, "$(mCol[" & mColArrIndex & "])", Rellenar_SpaceR( _
        IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString), _
        mColLength(mColArrIndex), " ") & vbTab, , 1)
    Next i
    
    If OriginalRsPosition = -1 And Not OnlyCurrentRow Then
        Set pRsValues = pRs
        If Data Then
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    RowNumCount = 0
    
    Do While Not pRsValues.EOF
        
        RsPreview = RsPreview & GetTab(NestLevel) & vbNewLine
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            RsPreview = RsPreview & GetTab(NestLevel) & Rellenar_SpaceR(TmpRsValue, mColLength(0), " ") & vbTab
        End If
        
        For i = 0 To pRsValues.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRsValues, pRsValues.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
            If Not UCase(TypeName(TmpRsValue)) = UCase("Recordset") Then
                RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & Rellenar_SpaceR(TmpRsValue, mColLength(mColArrIndex), " ") & vbTab
            Else
                RsPreview = RsPreview & vbNewLine & PrintRecordset(TmpRsValue, pRowNumAlias, OnlyCurrentRow, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel + 1)
            End If
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRsValues.MoveNext
        
    Loop
    
    PrintRecordset = RsPreview
    
Finally:
    
    On Error Resume Next
    
    If Data And Not OnlyCurrentRow Then
        If OriginalRsPosition <> -1 Then
            If pRs.CursorType = adOpenForwardOnly Then
                pRs.Requery
                pRs.Move OriginalRsPosition, 0
            Else
                SafePropAssign pRs, "Bookmark", OriginalRsPosition
            End If
        Else
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    Exit Function
    
FuncError:
    
    'Resume ' Debug
    
    PrintRecordset = vbNullString
    
    Resume Finally
    
End Function

' Imprime el Record Actual de un Recordset.

Public Function PrintRecord(ByVal pRs As ADODB.Recordset, _
Optional EncloseHeaders As Boolean = True, Optional LineBeforeData As Boolean = False, _
Optional ByVal pOnValueError As String = "Error|N/A") As String
    PrintRecord = PrintRecordset(pRs, , True, EncloseHeaders, LineBeforeData, pOnValueError)
End Function

' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

Public Function GetServerName(pCn As ADODB.Connection) As String
    
    On Error GoTo ErrorSrv
    
    GetServerName = pCn.Execute("SELECT @@SERVERNAME AS ServerName")!ServerName
    
    Exit Function
    
ErrorSrv:
    
    GetServerName = vbNullString
    
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim DDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                DDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - DDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   DDate = DateAdd("s", -1, DDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(DDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                DDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - DDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   DDate = DateAdd("s", -1, DDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(DDate, "YYYY-MM-DD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' To attempt to add Item + Key and ignore error in case Key is already contained in the collection.
' Ignore duplication error.

Public Function Collection_SafeAddKey(pCollection As Collection, ByVal pValue, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Add pValue, pKey
    
    Collection_SafeAddKey = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_EncontrarValor(pCollection As Collection, ByVal pValor, Optional ByVal pIndiceInicial As Long = 1) As Long
    Collection_EncontrarValor = Collection_FindValueIndex(pCollection, pValor, pIndiceInicial)
End Function

' How to Use:

' 1) To Know if Value Exists:

'If Collection_FindValueIndex(pCollection, pValue) <> -1 Then

' 2) To Search All Instances (Indexes) of Value in the Collection. Quick Example:

' SearchIndex = Collection_FindValueIndex(pCollection, pValue) ' Start Looking from Index 1

' If SearchIndex <> -1 Then
    ' This should be in a kind of While Loop...
    ' Keep searching other matches starting from next position:
    'SearchIndex = Collection_FindValueIndex(pCollection, pValue, SearchIndex + 1)
    ' Until Search Index = -1
' Else
    ' Next Match not found...
' End if

Public Function Collection_FindValueIndex(pCollection As Collection, ByVal pValue, Optional ByVal pStartIndex As Long = 1) As Long
    
    On Error GoTo Error
    
    Dim i As Long
    
    Collection_FindValueIndex = -1
    
    For i = pStartIndex To pCollection.Count
        If SafeEquals(pCollection.Item(i), pValue) Then
            Collection_FindValueIndex = i
            Exit Function
        End If
    Next i
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteKey(pCollection As Collection, ByVal pKey As String, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteKey = Collection_HasKey(pCollection, pKey)
End Function

' Collection_ExisteKey V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pKey, Null)) Then ' Key might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pKey) ' If this doesn't Proc Error Key is Null, but exists.
        Collection_HasKey = True
    Else ' Key Exists AND <> NULL
        Collection_HasKey = True
    End If
    
    Exit Function
        
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasKey = False ' Only on error, Key is not contained in the Collection.
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteIndex(pCollection As Collection, ByVal pIndex As Long, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteIndex = Collection_HasIndex(pCollection, pIndex)
End Function

' Collection_ExisteIndex V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pIndex, Null)) Then ' Index might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pIndex) ' If this doesn't Proc Error Index also exists.
        Collection_HasIndex = True
    Else ' Index Exists AND <> NULL
        Collection_HasIndex = True
    End If
    
    Exit Function
    
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasIndex = False ' Only on error Index is not contained in the Collection.
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean
    Collection_RemoveKey = Collection_SafeRemoveKey(pCollection, pKey)
End Function

' Ignore error even if the Key is not contained in the Collection.

Public Function Collection_SafeRemoveKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pKey
        
    Collection_SafeRemoveKey = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveKey = False ' Non - existant.
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    Collection_RemoveIndex = Collection_SafeRemoveIndex(pCollection, pIndex)
End Function

' Ignore error even if the Index is not contained in the Collection.

Public Function Collection_SafeRemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pIndex
        
    Collection_SafeRemoveIndex = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveIndex = False ' Non - existant.
    
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacion = New Collection
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(Item), ":", 2)               'Izq:Clave        'Der:Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        Call Collection_SafeAddKey(ConvertirCadenaDeAsociacion, ParClaveValor(1), ParClaveValor(0))
        
    Next
    
    If ConvertirCadenaDeAsociacion.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacion = Nothing
    
End Function

Public Function ConvertirCadenaDeAsociacionAvanzado(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Dictionary
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacionAvanzado = New Dictionary
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(Item), pSeparadorInterno, 2) ' Izq -> Clave ' Der -> Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        If Not ConvertirCadenaDeAsociacionAvanzado.Exists(ParClaveValor(0)) Then
            ConvertirCadenaDeAsociacionAvanzado.Add ParClaveValor(0), ParClaveValor(1)
        End If
        
    Next
    
    If ConvertirCadenaDeAsociacionAvanzado.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacionAvanzado = Nothing
    
End Function

Public Function ExportarCadenaDeAsociacion(ByVal pCollection As Collection, _
Optional ByVal pSeparador As String = "|") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacion = Empty
    
    Dim Item
    
    If pCollection.Count <= 0 Then Exit Function
    
    For Each Item In pCollection
        ExportarCadenaDeAsociacion = ExportarCadenaDeAsociacion & _
        pSeparador & Item
    Next
    
    ExportarCadenaDeAsociacion = Mid(ExportarCadenaDeAsociacion, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacion = Empty
    
End Function

Public Function ExportarCadenaDeAsociacionAvanzado(ByVal pValues As Dictionary, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
    Dim ItemKey, KeysCollection
    
    Set KeysCollection = AsEnumerable(pValues.Keys())
    
    If KeysCollection.Count <= 0 Then Exit Function
    
    For Each ItemKey In KeysCollection
        ExportarCadenaDeAsociacionAvanzado = ExportarCadenaDeAsociacionAvanzado & _
        pSeparador & ItemKey & pSeparadorInterno & pValues.Item(ItemKey)
    Next
    
    ExportarCadenaDeAsociacionAvanzado = Mid(ExportarCadenaDeAsociacionAvanzado, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
End Function

Public Function SystemDecSymbol() As String
    Dim TestFloatingNumber As String
    TestFloatingNumber = CStr((0 + (1 / 2)))
    For i = 1 To Len(TestFloatingNumber)
        If Not Mid(TestFloatingNumber, i, 1) Like "[0-9]" Then
            SystemDecSymbol = SystemDecSymbol & Mid(TestFloatingNumber, i, 1)
        End If
    Next
    If SystemDecSymbol = vbNullString Then SystemDecSymbol = "."
End Function

Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function FixTSQL(ByVal pCadena As String) As String
    FixTSQL = Replace(pCadena, "'", "''")
End Function

' Safe Val() - Sin el problema de que Val no soporta numeros formateados.
' Ej: Val("10,000.00") = 10 | SVal("10,000.00") = 10000 ' Como debe ser.
Public Function SVal(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 0) As Double
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SVal = CDbl(pExpression)
    Else
        SVal = pDefaultValue
    End If
End Function

' Min Val()
Public Function MinVal(ByVal pExpression As Variant, _
Optional ByVal pMinVal As Double = 0) As Double
    MinVal = SVal(pExpression, pMinVal)
    MinVal = IIf(MinVal < pMinVal, pMinVal, MinVal)
End Function

' Max Val()
Public Function MaxVal(ByVal pExpression As Variant, _
Optional ByVal pMaxVal As Double = 0) As Double
    MaxVal = SVal(pExpression, MaxVal)
    MaxVal = IIf(MaxVal > pMaxVal, pMaxVal, MaxVal)
End Function

' Min Val > 0
Public Function MinVal0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 1) As Double
    MinVal0 = SVal(pExpression, pDefaultValue)
    MinVal0 = IIf(MinVal0 > 0, MinVal0, pDefaultValue)
End Function

Public Function SDec(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 0) As Variant
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SDec = CDec(pExpression)
    Else
        SDec = CDec(pDefaultValue)
    End If
End Function

Public Function RoundUp(ByVal pNum As Variant, Optional ByVal pDecimals As Long) As Double
    RoundUp = CDbl(FormatNumber(pNum, pDecimals, vbTrue, vbFalse, vbFalse))
End Function

Public Function ExecuteSafeSQL(ByVal SQL As String, _
pCn As ADODB.Connection, _
Optional ByRef Records = 0, _
Optional ByVal ReturnsResultSet As Boolean = False, _
Optional ByVal DisconnectedRS As Boolean = False) As ADODB.Recordset
    
    On Error GoTo Error
    
    If ReturnsResultSet Then
        If DisconnectedRS Then
            Set ExecuteSafeSQL = New ADODB.Recordset
            ExecuteSafeSQL.CursorLocation = adUseClient
            ExecuteSafeSQL.Open SQL, pCn, adOpenStatic, adLockReadOnly
            Set ExecuteSafeSQL.ActiveConnection = Nothing
            Records = ExecuteSafeSQL.RecordCount
        Else
            Set ExecuteSafeSQL = pCn.Execute(SQL, Records)
        End If
    Else
        pCn.Execute SQL, Records
        Set ExecuteSafeSQL = Nothing
    End If
    
    Exit Function
    
Error:
    
    Set ExecuteSafeSQL = Nothing
    Records = -1
    
End Function

Public Function SBool(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = False) As Boolean
    On Error Resume Next
    ' Todo n�mero (Int, Double, Decimal) <> es True, 0 es False
    ' Todo String "1", "True", "true" es True, "0", "False", "false" es False
    ' Todo String no num�rico o pExpression que no se pueda convertir a Boolean da Error.
    ' En este caso se retorna pDefaultValue
    SBool = CBool(pExpression)
    If Err.Number <> 0 Then
        SBool = pDefaultValue
    End If
End Function

Public Function ValorNumericoOpcion(ByVal pBit As Double) As Double: ValorNumericoOpcion = (2 ^ pBit): End Function

Private Function ConvertirBinario(ByVal pNumeroCombinacion As Double, Optional ByVal pTopeCantidadOpciones As Double = 7) As String
    
    For i = pTopeCantidadOpciones - 1 To 0 Step -1
        ' Check for Bitwise Operation (Comparar Bits)
        ' Ej: (114 And 8) = False, (114 And 16) = True
        ConvertirBinario = IIf(pNumeroCombinacion And (2 ^ i), "1", "0") & ConvertirBinario
    Next i
    
End Function

Public Function ArrayBitsSeleccionados(ByVal pNumeroCombinacion As Double, Optional ByVal pTopeCantidadOpciones As Double = 7) As Variant
    
    ' Esta funci�n devuelve un Array con las opciones indicando cuales estan seleccionadas y cuales no.
    
    ' pNumeroCombinacion es el numero equivalente a la combinaci�n de las opciones seleccionadas.
    ' pTopeCantidadOpciones es el n�mero de opciones que existen.
    ' Por defecto esta en 7, por ejemplo, para el Caso D�as de la Semana (Son 7).
    
    ' La idea de esto es guardar en Base de Datos un N�mero en vez de un campo por cada opci�n.
    ' Ejemplo con una Tabla: TAREAS_PROGRAMADAS. Campo NUMERIC(18,0) DiasEjecucion.
    ' En dicho campo guardamos pNumeroCombinacion, en vez de tener en la tabla 7 campos Booleanos:
    ' Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo...
    
    ' La din�mica es la siguiente, cada d�a tiene un n�mero secuencial representado por 2 ^ N:
    ' Lunes = 1, Martes = 2, Miercoles = 4, Jueves = 8, Viernes = 16, Sabado = 32, Domingo = 64
    ' Si por ejemplo se seleccionan Martes, Sabado y Domingo, pNumeroCombinacion = (2^2 + 2^5 + 2^6) = 100
    ' El resultado de esta funci�n para los Par�metros pNumeroCombinacion = 100, pTopeCantidadOpciones = 7 ser�
    ' El siguiente Array:
    
    ' ArrayBitsSeleccionados(0)= "0" ' Lunes
    ' ArrayBitsSeleccionados(1)= "1" ' Martes       ' (2^1)
    ' ArrayBitsSeleccionados(2)= "0" ' Miercoles
    ' ArrayBitsSeleccionados(3)= "0" ' Jueves
    ' ArrayBitsSeleccionados(4)= "0" ' Viernes
    ' ArrayBitsSeleccionados(5)= "1" ' Sabado       ' (2^5)
    ' ArrayBitsSeleccionados(6)= "1" ' Domingo      ' (2^6)
    
    ' Para acceder a una comprobaci�n directa sobre un bit en espec�fico en vez de buscar todo el arreglo:
    ' llamar a la funci�n BitActivo(pNumeroOpcion, , , pNumeroCombinacion).
    
    ' Por ejemplo para saber si el Jueves esta marcado en pNumeroCombinacion = 65:
    
    ' JuevesActivo = BitActivo(4, , , 65) ' (False). En Cambio:
    ' JuevesActivo = BitActivo(4, , , 58) ' (True)
    
    ' ----------------------------------------------------------------------------------------------------------
    
    Dim mArrayNumeros()
    Dim mNumSelBin As String
    
    mNumSelBin = ConvertirBinario(pNumeroCombinacion, pTopeCantidadOpciones)
    
    For i = 1 To Len(mNumSelBin)
        ReDim Preserve mArrayNumeros(i - 1)
        mArrayNumeros(i - 1) = Mid(mNumSelBin, i, 1)
    Next i
    
    ArrayBitsSeleccionados = mArrayNumeros
    
End Function

Public Function BitActivo(ByVal pNumeroOpcion As Double, Optional ByVal BitArrayCargado As Variant, _
Optional ByVal pNumeroCombinacion As Double, Optional ByVal pTopeCantidadOpciones As Double = 7) As Boolean
    On Error Resume Next
    If IsMissing(BitArrayCargado) Then BitArrayCargado = ArrayBitsSeleccionados(pNumeroCombinacion, pTopeCantidadOpciones)
    BitActivo = CBool(Val(BitArrayCargado(pNumeroOpcion)))
End Function
